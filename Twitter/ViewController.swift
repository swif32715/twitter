//
//  ViewController.swift
//  Twitter
//
//  Created by N Tek Yazılım on 17.07.2017.
//  Copyright © 2017 Hamdullah KALAY. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnKaydolRadius: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        btnKaydolRadius.layer.cornerRadius = 8
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

